/// <reference path="definitions/node/node.d.ts" />
/// <reference path="./mymodule.ts" />
/// <reference path="definitions/ws/ws.d.ts" />
/// <reference path="./shared/messaging__server.ts" />

import http = require("http")
import mymodule = require('./mymodule')
import ws = require("ws")
import fs = require("fs")
import msg = require('./shared/messaging__server')

// mymodule.testFunction()
// var mc = new mymodule.MyClass()
// mc.myMethod()

/**
 * ეს არის დინამიკური ინტერფესისი რომელიც გამოიყენება client-ზე
 * დამატებითი მონაცემების მისამატებლად 
 */
interface WebSocketClientEx extends ws
{
	customData:{clientBelongsToRoom:Room}
}

class Room
{
	clients:Array<ws> = new Array<ws>()
}

interface RoomsCollection {   
   [roomId: number]: Room;
}

var rooms:RoomsCollection = {}

//ოთახის ნომერი საიდანაც იწყება ათვლა
const MINIMUM_ROOM_ID = 1000

var currentRoomIndex = MINIMUM_ROOM_ID
var messageHandlers:msg.Messaging.ServerMessageHandlersCollection = {}

messageHandlers[msg.Messaging.ServerOperations.CREATE_ROOM] = (client:ws, data:msg.Messaging.ServerMessage) => 
{
	var clientEx = <WebSocketClientEx><any>client
	
	//თუ კლიენტი უკვე ასოცირებულია რომელიმე მაგიდასთან მაშნ ახალი მაგიდის მოთხოვნას არ ვუგულებელვყოფ
	if(clientEx.customData != null && clientEx.customData.clientBelongsToRoom != null)
	{
		return
	}
	
	//ვქმნი ოთახს და ვამატებ მიმდინარე კლიენტს მის სიაში
	rooms[currentRoomIndex] = new Room()
	rooms[currentRoomIndex].clients.push(client)
	
	clientEx.customData = { clientBelongsToRoom: rooms[currentRoomIndex] }
	
	//კლიენტს ვუგზავნი ამ ოთახის იდენტიფიკატორს, იმისთვის რომ დავუშვათ სხვას უკვე ეს იდენტიფიკატორი გაუგზავნოს მოსაწვევად
	client.send( JSON.stringify( <msg.Messaging.ServerMessage>{
		serverOperation: msg.Messaging.ServerOperations.ROOM_ID,
		data:{
			roomId : currentRoomIndex
		}
	}))
	
	currentRoomIndex++
	
}

messageHandlers[msg.Messaging.ServerOperations.JOIN_ROOM] = (client:ws, data:msg.Messaging.ServerMessage) => {
	var clientEx = <WebSocketClientEx><any>client
	var messageJoinRoom = <msg.Messaging.ServerMessageJoinRoom>data
	
	//თუ კლიენტი უკვე ასოცირებულია რომელიმე მაგიდასთან მაშნ ახალი მაგიდის მოთხოვნას არ ვუგულებელვყოფ
	if(clientEx.customData != null && clientEx.customData.clientBelongsToRoom != null)
	{
		return
	}
		
	var roomId = messageJoinRoom.data.roomId
	var roomForId = rooms[roomId]
	
	//თუ, მოცემულ roomId-ს მქონე ოთახი არსებობს მაშინ გრძელდება დამუშავება
	if( roomForId )
	{
		//კლიენტს გავუწერ რომელ ოთახშია
		clientEx.customData = { clientBelongsToRoom: roomForId }
		
		//კლიენტს დავამატებ ოთახში
		roomForId.clients.push(clientEx)
		
		//და გავუგზავნი უსტარს რომ კარგად დაემატა ოთახში
		var message:msg.Messaging.ServerMessage = {
			serverOperation : msg.Messaging.ServerOperations.JOIN_ROOM_SUCCESS
		}
		
		client.send(JSON.stringify(message))
		
		//ახლა ყველა სხვა კლიენტს მოცემულ ოთახში დავუგზავნი შეტყობინებას რომ ახალი კლიენტი შემოუერთდა ოთახს
		message = {
			serverOperation : msg.Messaging.ServerOperations.CLIENT_JOINED_ROOM
		}
		
		var stringifiedMessage = JSON.stringify(message)
		
		for(var clientIndex = 0; clientIndex < roomForId.clients.length; clientIndex++)
		{
			var roomClient = roomForId.clients[clientIndex]
			
			if( roomClient != clientEx )
			{
				roomClient.send(stringifiedMessage)
			}
		}
	}
}

messageHandlers[msg.Messaging.ServerOperations.WEBRTC_SDP] =  (client:ws, data:msg.Messaging.ServerMessage) => {
	
	relayMessageToOtherClients(client, data)
		
}

messageHandlers[msg.Messaging.ServerOperations.RELAY_MESSAGE] = (client:ws, data:msg.Messaging.ServerMessage) => {
	
	relayMessageToOtherClients(client, data)
		
}

ws.createServer( {port:8090}, (client:ws) => {
	
	client.on("message", (data: any, flags: {binary: boolean}):void => {
		//იპარსება შემოსული მესიჯი
		var messageJSON:msg.Messaging.ServerMessage = JSON.parse(data)
		
		//გავარსვის შემდეგ ხდება მესიჯის შესაბამისი ოპერაციის შესრულება
		messageHandlers[messageJSON.serverOperation](client, messageJSON)
	})
	
})

/**
 * მესიჯის გადამმისამართებელის განზობადებული ვარიანტი.
 * 
 * ეს გამოიყენება მაგალითად WebRTC-ს SDP მონაცემების გადასაგზავნად.
 */
function relayMessageToOtherClients(fromClient:ws, data:msg.Messaging.ServerMessage) {
	
	var clientEx = <WebSocketClientEx>fromClient
	var stringifiedMessage = JSON.stringify(data)
	
	if(clientEx.customData != undefined && clientEx.customData.clientBelongsToRoom != null) {
		
		var clientRoom = clientEx.customData.clientBelongsToRoom
		
		for(var clientIndex = 0; clientIndex < clientRoom.clients.length; clientIndex++)
		{
			var roomClient = clientRoom.clients[clientIndex]
			
			if( roomClient != clientEx )
			{
				roomClient.send(stringifiedMessage)
			}
		}
		
	}
	
}


http.createServer((req,res) => {
	
	var fileContents:Buffer = new Buffer("")
	
	if(req.url == "/")
	{
		fileContents = fs.readFileSync("./client/index.html")
	}
	
	if(req.url == "/client.js")
	{
		fileContents = fs.readFileSync("./client"+req.url)
	}
	
	if(req.url == "/adapter.js")
	{
		fileContents = fs.readFileSync("./client"+req.url)
	}	
	
	res.write(fileContents)
	res.end()
	
}).listen(8080)
