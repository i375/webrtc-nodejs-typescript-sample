var Messaging;
(function (Messaging) {
    /**
     * სერვერს და კლიენტს შორის გაცვლადი ბრძანებების ნუსხა
     */
    var ServerOperations = (function () {
        function ServerOperations() {
        }
        ServerOperations.CREATE_ROOM = "CR";
        ServerOperations.JOIN_ROOM = "JR";
        ServerOperations.RELAY_MESSAGE = "RM";
        ServerOperations.RELAYED_MESSAGE = "RLDM";
        ServerOperations.ROOM_ID = "RID";
        ServerOperations.JOIN_ROOM_SUCCESS = "JRS";
        /**
         * ეს გამოიყენება იმისთვის რომ უკვე შემოერთებულ კლიენტს
         * გაეგზავნოს შეტყობინება იმაზე რომ სხვა ახალი კლიენტი დაემატა ოთახს.
         */
        ServerOperations.CLIENT_JOINED_ROOM = "CJR";
        /**
         * WebRTC შეერთებისას მეორე ბოლოში მყოფი კლიენტის SDP
         * ამ მესიჯს სერვერი RELAY_MESSAGES-ს მაგვარად უმისამართებს დანარჩენ კლიენტებს
         */
        ServerOperations.WEBRTC_SDP = "WBRTCSDP";
        return ServerOperations;
    })();
    Messaging.ServerOperations = ServerOperations;
})(Messaging = exports.Messaging || (exports.Messaging = {}));
