/// <reference path="../definitions/ws/ws.d.ts" />
import ws = require("ws")

export module Messaging
{

	export interface ServerMessage
	{
		serverOperation:string
	}
	
	/**
	 * გამოიყენება WebRTC-ს SDP-ს გადასაგზავნად.
	 */
	export interface ServerMessageSDPString extends ServerMessage
	{
		data:{
			sdp:string,
			type:string
		}
	}	
	
	export interface ServerMessageJoinRoom extends ServerMessage
	{
		data:{
			roomId:number
		}
	}
	
	/**
	 * სერვერს და კლიენტს შორის გაცვლადი ბრძანებების ნუსხა
	 */
	export class ServerOperations
	{
		static CREATE_ROOM:string = "CR"
		static JOIN_ROOM:string = "JR"
		static RELAY_MESSAGE:string = "RM"
		static RELAYED_MESSAGE: string = "RLDM"
		static ROOM_ID:string = "RID"	
		static JOIN_ROOM_SUCCESS = "JRS"
		
		/**
		 * ეს გამოიყენება იმისთვის რომ უკვე შემოერთებულ კლიენტს
		 * გაეგზავნოს შეტყობინება იმაზე რომ სხვა ახალი კლიენტი დაემატა ოთახს.
		 */
		static CLIENT_JOINED_ROOM = "CJR"
		
		/**
		 * WebRTC შეერთებისას მეორე ბოლოში მყოფი კლიენტის SDP
		 * ამ მესიჯს სერვერი RELAY_MESSAGES-ს მაგვარად უმისამართებს დანარჩენ კლიენტებს
		 */
		static WEBRTC_SDP = "WBRTCSDP"		
	}
	
	export interface ServerMessageHandlersCollection {
		[messageName: string]: ServerMessageHandler;
	}

	export interface ServerMessageHandler {
		(client:ws, data:any): void;
	}
}