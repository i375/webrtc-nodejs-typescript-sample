var Messaging;
(function (Messaging) {
    /**
     * სერვერს და კლიენტს შორის გაცვლადი ბრძანებების ნუსხა
     */
    var ServerOperations = (function () {
        function ServerOperations() {
        }
        ServerOperations.CREATE_ROOM = "CR";
        ServerOperations.JOIN_ROOM = "JR";
        ServerOperations.RELAY_MESSAGE = "RM";
        ServerOperations.RELAYED_MESSAGE = "RLDM";
        ServerOperations.ROOM_ID = "RID";
        ServerOperations.JOIN_ROOM_SUCCESS = "JRS";
        /**
         * ეს გამოიყენება იმისთვის რომ უკვე შემოერთებულ კლიენტს
         * გაეგზავნოს შეტყობინება იმაზე რომ სხვა ახალი კლიენტი დაემატა ოთახს.
         */
        ServerOperations.CLIENT_JOINED_ROOM = "CJR";
        /**
         * WebRTC შეერთებისას მეორე ბოლოში მყოფი კლიენტის SDP
         * ამ მესიჯს სერვერი RELAY_MESSAGES-ს მაგვარად უმისამართებს დანარჩენ კლიენტებს
         */
        ServerOperations.WEBRTC_SDP = "WBRTCSDP";
        /**
         *
         */
        ServerOperations.WEBRTC_ICE_CANDIDATE = "WBRTCICEC";
        return ServerOperations;
    })();
    Messaging.ServerOperations = ServerOperations;
})(Messaging || (Messaging = {}));
/// <reference path="../shared/messaging__client.ts" />
/// <reference path="../definitions/webrtc/RTCPeerConnection.d.ts" />
var domElements = {
    logDiv: null,
    messageText: null
};
var webRTC_STUN_serversList = {
    iceServers: [
        { "url": "stun:stun.l.google.com:19302" },
        { "url": "stun:stun.stunprotocol.prg" },
        { "url": "stun:stun.callwithus.com" },
        { "url": "stun:stun.counterpath.net" },
        { "url": "stun:stun.internetcalls.com" },
        { "url": "stun:stun.noc.ams-ix.net" },
        { "url": "stun:stun.phoneserve.com" },
        { "url": "stun:stun.sipgate.net" },
    ]
};
var MINIMUM_ROOM_ID = 1000;
var messageHandlers = {};
var webRTCMasterClient = false;
messageHandlers[Messaging.ServerOperations.JOIN_ROOM_SUCCESS] = function (data) {
    //ეს არის პირველი, master კლიენტი, რომელიმაც შექმნა ოთახიც და პირველ შეთავაზებასაც ეს აგზავნის
    webRTCMasterClient = false;
    establishRTCDataChannelConnection(webRTCMasterClient);
};
messageHandlers[Messaging.ServerOperations.CLIENT_JOINED_ROOM] = function (data) {
    //ეს არის slave, რომელიც master-ის მიერ შექმნილ ოთახზე მიერთდა და ელოდება master-ის შეთავაზებას რაზეც უფასუხებს პაუსხით,
    //რაზეც თავის მხრივ master უპასუხებს პასუხით.
    webRTCMasterClient = true;
    establishRTCDataChannelConnection(webRTCMasterClient);
};
messageHandlers[Messaging.ServerOperations.ROOM_ID] = function (data) {
};
messageHandlers[Messaging.ServerOperations.WEBRTC_SDP] = function (data) {
    //logMessageInLogDiv(data)
    //აღვადგენ მეორე კლიენტის SDP ობიექტს, მისგან გამოზავნილი SDP string-დან
    var rtcRemoteSessionDescription = new RTCSessionDescription({
        sdp: data.data.sdp,
        type: data.data.type
    });
    if (data.data.type == "offer") {
        //მეორე კლიენტისგან გამოგზავნილ offer ტიპის SDP-ს ვსაზღვრად როგორც მოშორებულ SDP-ს	
        rtcPeerConnection.setRemoteDescription(rtcRemoteSessionDescription, function () {
            logMessageInLogDiv("Remote SDP set");
            //თუ მეორე კლიენისგან მოსული SDP offer ტიპის არის, საპასუხო answer ტიპის SDP-ს ვაგენერირებ და ვაგზავნილ
            rtcPeerConnection.createAnswer(function (sdp) {
                logMessageInLogDiv("Answer SDP created and set as local. <br/>Now will wait for ICE candidates to gather and then will send local SDP to remote client.");
                //ჩემს answer ტიპის SDP-ს ჩემთვის ვგზასღვრავ როგორც ლოკალურს
                rtcPeerConnection.setLocalDescription(sdp);
            });
        }, function (errorInformation) {
            console.log(errorInformation);
            logMessageInLogDiv("Setting remote SDP failed");
        });
    }
    else if (data.data.type == "answer") {
        //მეორე კლიენტისგან გამოგზავნილ answer ტიპის SDP-ს ვსაზღვრად როგორც მოშორებულ SDP-ს	
        rtcPeerConnection.setRemoteDescription(rtcRemoteSessionDescription, function () {
            logMessageInLogDiv("Remote SDP set.");
        }, function (errorInformation) {
            console.log(errorInformation);
            logMessageInLogDiv("Setting remote SDP failed");
        });
    }
};
var rtcPeerConnection = null;
var rtcDataChannel = null;
var RTC_SEND_CHANNEL = "RTC_SEND_CHANNEL";
var dataChannelOptions = {
    ordered: false,
    maxRetransmitTime: 3000
};
/**
 * აქ დამყარდება კავშირი უკვე WebRTC-ს გავლით.
 * პლუს ეს პროცედურა შედარებით დამოუკიდებელი უნდა იყოს WebSocket-ზე დავუძვნებული სასიგნალო მექანიზმისგან
 * ანუ საშუამავლო state პარამეტრების მიხედვით უნდა ვმართო, კოდში უკეთ ჩანს ეს.
 */
function establishRTCDataChannelConnection(master) {
    if (master) {
        logMessageInLogDiv("IS MASTER");
    }
    else {
        logMessageInLogDiv("IS SLAVE");
    }
    logMessageInLogDiv("Establishing WebRTC connection");
    //აქ ვიწყებ RTC შეერთების დამყარებას
    rtcPeerConnection = new RTCPeerConnection(webRTC_STUN_serversList);
    //RTCDataChannel-ის შექმნა, master client-ის მხრიდან
    if (master) {
        rtcDataChannel = rtcPeerConnection.createDataChannel(RTC_SEND_CHANNEL);
        rtcDataChannel.onerror = function (e) {
            logMessageInLogDiv(e.toString());
        };
        rtcDataChannel.onopen = function (e) {
            logMessageInLogDiv("RTCDataChannel open at MASTER client");
        };
        rtcDataChannel.onmessage = function (e) {
            console.log("Message on RTC data channel");
            console.log(e.data);
        };
    }
    else if (master == false) {
        rtcPeerConnection.ondatachannel = function (event) {
            logMessageInLogDiv("RTCDataChannel become available");
            rtcDataChannel = event.channel;
        };
    }
    // ! რატომღაც არ გამოიძახება და გასარკვევია, შეიძლება localhost-ზე რომ ხდება მაგის ბრალი იყოს.
    rtcPeerConnection.onicecandidate = function (event) {
        console.log(event.candidate);
        //ICE კანდიდატებს ვამატებ ლოკალურ SDP
        if (event.candidate != null) {
            logMessageInLogDiv("ICE candidate available");
            rtcPeerConnection.addIceCandidate(event.candidate, function () {
                logMessageInLogDiv("ICE candidate added successfully to local SDP");
            }, function (errorInformation) {
                logMessageInLogDiv("ICE candidate, failed to add to local SDP");
            });
        }
        else if (event.candidate == null) {
            logMessageInLogDiv("ICE candidate gathering finished");
            console.log(rtcPeerConnection.localDescription);
            sendLocalSDPToOtherPeer();
        }
    };
    if (master) {
        //ვქმნი პირველ შეთავაზებას SDP-თურთ და ვუგზავნი მეორე კლიენტს
        rtcPeerConnection.createOffer(function (sdp) {
            //ჩემს SDP-ს ვსაზღვრავ როგორც ლოკალურ SDP-ს
            rtcPeerConnection.setLocalDescription(sdp, function () {
                logMessageInLogDiv("Local SDP set.<br/>Now will wait for ICE candidates to gather and then will send local SDP to remote client.");
            });
        }, function (errorInformation) {
            logMessageInLogDiv(errorInformation.toString());
        });
    }
}
/**
 *
 * ფუნქცია გამოიყენება ლოკალური SDP-ს გასაგზავნად მეორე კლიენტთან,
 * მას შემდეგ რაც ICE კანდიდატები შეგროვდა და დაემატა ლოკალურ SDP-ს.
 *
 */
function sendLocalSDPToOtherPeer() {
    logMessageInLogDiv("Sending local SDP to remote client.");
    var sdpMessage = {
        serverOperation: Messaging.ServerOperations.WEBRTC_SDP,
        data: {
            sdp: rtcPeerConnection.localDescription.sdp,
            type: rtcPeerConnection.localDescription.type
        }
    };
    //ვუგზავნი ჩემს SDP-ს მეორე კლიენტს
    ws.send(JSON.stringify(sdpMessage));
}
function logMessageInLogDiv(message) {
    domElements.logDiv.innerHTML = domElements.logDiv.innerHTML + "<br/>-<br/>" + message;
}
var ws;
function main() {
    ws = new WebSocket("ws://" + window.location.hostname + ":8090");
    var socketConnected = false;
    ws.onopen = function (e) {
        socketConnected = true;
    };
    ws.onmessage = function (e) {
        logMessageInLogDiv("WebSocket message: " + e.data);
        //იპარსება შემოსული მესიჯი
        var messageJSON = JSON.parse(e.data);
        //გავარსვის შემდეგ ხდება მესიჯის შესაბამისი ოპერაციის შესრულება
        messageHandlers[messageJSON.serverOperation](messageJSON);
    };
    var roomIdText = document.getElementById("roomIdText");
    var logText = document.getElementById("logText");
    var connectOrCreatenewRoom = document.getElementById("connectOrCreatenewRoom");
    var sendMessageButton = document.getElementById("sendMessageButton");
    domElements.logDiv = document.getElementById("logDiv");
    domElements.messageText = document.getElementById("messageText");
    sendMessageButton.addEventListener("click", function (e) {
        //მინიმალური შემოწმებით(ოღონდ რაიმე მაინც ეწეროს ეწეროს), ვაგზავნი სერვერზე
        if (domElements.messageText.value.length > 10) {
            ws.send(domElements.messageText.value);
        }
    });
    connectOrCreatenewRoom.addEventListener("click", function (e) {
        if (socketConnected == false)
            return;
        var roomId = parseInt(roomIdText.value);
        //თუ ოთახის იდენტიფიკატორი არ არის შეყვანილი ან არის არასწორად 
        //მითითებული მაშნ გაიგზავნოს ახალი ოთახის შექმნის მოთხოვნა
        if (roomIdText.value.length < 2 || roomId < MINIMUM_ROOM_ID) {
            var message = {
                serverOperation: Messaging.ServerOperations.CREATE_ROOM
            };
            ws.send(JSON.stringify(message));
        }
        else {
            var message = {
                serverOperation: Messaging.ServerOperations.JOIN_ROOM,
                data: {
                    roomId: roomId
                }
            };
            ws.send(JSON.stringify(message));
        }
    });
}
window.onload = function (e) {
    main();
};
