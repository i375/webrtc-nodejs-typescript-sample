// Execute: node contentServer.js
// In folder you want to server static content from.
// Enter http://localhost:8080 in your browser

var http = require("http");
var fs = require("fs");
http.createServer(function (req, res) {
    var fileContents = new Buffer("");
    var _url = req.url.replace("..", "");
    
    console.log(_url)
    
    if (_url == "/") {
        
        try
        {
            fileContents = fs.readFileSync("./index.html");
        }
        catch(e){}
                
    }
    else {
        
        try
        {
            fileContents = fs.readFileSync("." + _url);
        }
        catch(e){}
        
    }
    res.write(fileContents);
    res.end();
    
}).listen(8080);

