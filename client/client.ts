/// <reference path="../shared/messaging__client.ts" />
/// <reference path="../definitions/webrtc/RTCPeerConnection.d.ts" />

var domElements: {
	
	logDiv: HTMLDivElement
	messageText: HTMLTextAreaElement
	
} = { 
	
	logDiv: null,
	messageText: null
	
}

var webRTC_STUN_serversList:RTCConfiguration = {
	iceServers: [
		{ "url": "stun:stun.l.google.com:19302" },
		{ "url": "stun:stun.stunprotocol.prg" },
		{ "url": "stun:stun.callwithus.com" },
		{ "url": "stun:stun.counterpath.net" },
		{ "url": "stun:stun.internetcalls.com" },
		{ "url": "stun:stun.noc.ams-ix.net" },
		{ "url": "stun:stun.phoneserve.com" },
		{ "url": "stun:stun.sipgate.net" },
	]
}

const MINIMUM_ROOM_ID = 1000

var messageHandlers: Messaging.ServerMessageHandlersCollection = {}

var webRTCMasterClient : boolean = false

messageHandlers[ Messaging.ServerOperations.JOIN_ROOM_SUCCESS ] = ( data: Messaging.ServerMessage ) => {
	
	//ეს არის პირველი, master კლიენტი, რომელიმაც შექმნა ოთახიც და პირველ შეთავაზებასაც ეს აგზავნის
	webRTCMasterClient = false
	establishRTCDataChannelConnection( webRTCMasterClient )
	
}   

messageHandlers[ Messaging.ServerOperations.CLIENT_JOINED_ROOM ] = ( data:Messaging.ServerMessage ) => {
	
	//ეს არის slave, რომელიც master-ის მიერ შექმნილ ოთახზე მიერთდა და ელოდება master-ის შეთავაზებას რაზეც უფასუხებს პაუსხით,
	//რაზეც თავის მხრივ master უპასუხებს პასუხით.
	webRTCMasterClient = true
	establishRTCDataChannelConnection( webRTCMasterClient )
	
}

messageHandlers[ Messaging.ServerOperations.ROOM_ID ] = ( data:Messaging.ServerMessage ) => {
	
}

messageHandlers[ Messaging.ServerOperations.WEBRTC_SDP ] = ( data:Messaging.ServerMessageSDPString ) => {
	
	//logMessageInLogDiv(data)
	
	//აღვადგენ მეორე კლიენტის SDP ობიექტს, მისგან გამოზავნილი SDP string-დან
	var rtcRemoteSessionDescription = new RTCSessionDescription({
		
		sdp:data.data.sdp,
		type: data.data.type
		
	})
	
	if (data.data.type == "offer") {
	
		//მეორე კლიენტისგან გამოგზავნილ offer ტიპის SDP-ს ვსაზღვრად როგორც მოშორებულ SDP-ს	
		rtcPeerConnection.setRemoteDescription(rtcRemoteSessionDescription, () => {

			logMessageInLogDiv("Remote SDP set")
		
			//თუ მეორე კლიენისგან მოსული SDP offer ტიპის არის, საპასუხო answer ტიპის SDP-ს ვაგენერირებ და ვაგზავნილ

			rtcPeerConnection.createAnswer((sdp: RTCSessionDescription) => {

				logMessageInLogDiv("Answer SDP created and set as local. <br/>Now will wait for ICE candidates to gather and then will send local SDP to remote client.")
				
				//ჩემს answer ტიპის SDP-ს ჩემთვის ვგზასღვრავ როგორც ლოკალურს
				rtcPeerConnection.setLocalDescription(sdp)

			})

		}, ( errorInformation: DOMError ) => {
			
			console.log( errorInformation )
			logMessageInLogDiv( "Setting remote SDP failed" )
			
		})

	}
	else if (data.data.type == "answer") {
		
		//მეორე კლიენტისგან გამოგზავნილ answer ტიპის SDP-ს ვსაზღვრად როგორც მოშორებულ SDP-ს	
		rtcPeerConnection.setRemoteDescription( rtcRemoteSessionDescription, () => {

			logMessageInLogDiv("Remote SDP set.")
			
		}, ( errorInformation: DOMError ) => {
			
			console.log( errorInformation )
			logMessageInLogDiv( "Setting remote SDP failed" )
			
		})
				
	}
	
}

var rtcPeerConnection : webkitRTCPeerConnection = null
var rtcDataChannel : RTCDataChannel = null
var RTC_SEND_CHANNEL : string = "RTC_SEND_CHANNEL"

var dataChannelOptions = {
	
  ordered: false,          // do not guarantee order
  maxRetransmitTime: 3000, // in milliseconds
  
}

/**
 * აქ დამყარდება კავშირი უკვე WebRTC-ს გავლით.
 * პლუს ეს პროცედურა შედარებით დამოუკიდებელი უნდა იყოს WebSocket-ზე დავუძვნებული სასიგნალო მექანიზმისგან
 * ანუ საშუამავლო state პარამეტრების მიხედვით უნდა ვმართო, კოდში უკეთ ჩანს ეს.
 */
function establishRTCDataChannelConnection( master:boolean ) {
	
	if(master) { logMessageInLogDiv("IS MASTER") }
	else { logMessageInLogDiv("IS SLAVE") }
	
	logMessageInLogDiv( "Establishing WebRTC connection" )
	
	//აქ ვიწყებ RTC შეერთების დამყარებას
	rtcPeerConnection = new RTCPeerConnection( webRTC_STUN_serversList )
	
	//RTCDataChannel-ის შექმნა, master client-ის მხრიდან
	if ( master ) {
		
		rtcDataChannel = rtcPeerConnection.createDataChannel( RTC_SEND_CHANNEL )

		rtcDataChannel.onerror = ( e:Event ) => {
			
			logMessageInLogDiv( e.toString() )
			
		}
		
		rtcDataChannel.onopen =  ( e:Event ) => {
			
			logMessageInLogDiv("RTCDataChannel open at MASTER client")

		}

		rtcDataChannel.onmessage = (e: RTCMessageEvent) => {

			console.log("Message on RTC data channel")
			console.log(e.data)

		}

	}
	//RTCDataChannel-ის მიღება rtcPeerConnection-დან, slave client-ის მხარეს
	else if (master == false) {

		rtcPeerConnection.ondatachannel = (event: RTCDataChannelEvent) => {

			logMessageInLogDiv("RTCDataChannel become available")
			rtcDataChannel = event.channel

		}

	}
		
	// ! რატომღაც არ გამოიძახება და გასარკვევია, შეიძლება localhost-ზე რომ ხდება მაგის ბრალი იყოს.
	rtcPeerConnection.onicecandidate = ( event: RTCIceCandidateEvent ) => {
		
		console.log( event.candidate )
		
		//ICE კანდიდატებს ვამატებ ლოკალურ SDP
		if ( event.candidate != null ) {
			
			logMessageInLogDiv( "ICE candidate available" )
			
			rtcPeerConnection.addIceCandidate( event.candidate, () => {
				
				logMessageInLogDiv("ICE candidate added successfully to local SDP")
				
			}, (errorInformation: DOMError) => {
				
				logMessageInLogDiv("ICE candidate, failed to add to local SDP")
				
			} )
		}
		//როდესაც კანდიდატი null ეს ნიშნავს იმას რომ შეგროვება მორჩა
		//და SDP გაგზავნადია მეორე კლიეტთან
		else if ( event.candidate == null ) {
			
			logMessageInLogDiv("ICE candidate gathering finished")
			console.log(rtcPeerConnection.localDescription)
			sendLocalSDPToOtherPeer()
			
		}
		

		
	}
			
	if ( master ) {
		
		//ვქმნი პირველ შეთავაზებას SDP-თურთ და ვუგზავნი მეორე კლიენტს
		rtcPeerConnection.createOffer( ( sdp: RTCSessionDescription ) => {
	
			//ჩემს SDP-ს ვსაზღვრავ როგორც ლოკალურ SDP-ს
			rtcPeerConnection.setLocalDescription( sdp, () => {
				
				logMessageInLogDiv("Local SDP set.<br/>Now will wait for ICE candidates to gather and then will send local SDP to remote client.")
				
			})
	
		}, ( errorInformation: DOMError ) => {
	
				logMessageInLogDiv( errorInformation.toString() )
	
		})		
		
	}
	
}

/**
 * 
 * ფუნქცია გამოიყენება ლოკალური SDP-ს გასაგზავნად მეორე კლიენტთან,
 * მას შემდეგ რაც ICE კანდიდატები შეგროვდა და დაემატა ლოკალურ SDP-ს.
 * 
 */
function sendLocalSDPToOtherPeer() : void {
	
	logMessageInLogDiv("Sending local SDP to remote client.")
	
	var sdpMessage: Messaging.ServerMessageSDPString = {
		
		serverOperation: Messaging.ServerOperations.WEBRTC_SDP,
		
		data: {

			sdp: rtcPeerConnection.localDescription.sdp,
			type: rtcPeerConnection.localDescription.type

		}

	}

	//ვუგზავნი ჩემს SDP-ს მეორე კლიენტს
	ws.send(JSON.stringify(sdpMessage))	
		
}

function logMessageInLogDiv( message:string ) : void {
	
	domElements.logDiv.innerHTML = domElements.logDiv.innerHTML + "<br/>-<br/>" + message
	
}

var ws:WebSocket

function main() {

	ws = new WebSocket( "ws://" + window.location.hostname + ":8090" )
	
	var socketConnected = false
	
	ws.onopen = (e) => {
		
		socketConnected = true
		
	}
	
	ws.onmessage = (e) => {
		
		logMessageInLogDiv("WebSocket message: " + e.data)
		
		//იპარსება შემოსული მესიჯი
		var messageJSON:Messaging.ServerMessage = JSON.parse(e.data)
		
		//გავარსვის შემდეგ ხდება მესიჯის შესაბამისი ოპერაციის შესრულება
		messageHandlers[messageJSON.serverOperation](messageJSON)		
		
	}
	
	var roomIdText = <HTMLInputElement>document.getElementById("roomIdText") 
	var logText = <HTMLTextAreaElement>document.getElementById("logText")
	var connectOrCreatenewRoom = <HTMLButtonElement>document.getElementById("connectOrCreatenewRoom")
	var sendMessageButton = <HTMLButtonElement>document.getElementById("sendMessageButton")
	
	domElements.logDiv = <HTMLDivElement>document.getElementById("logDiv")
	domElements.messageText = <HTMLTextAreaElement>document.getElementById("messageText")
	
	sendMessageButton.addEventListener("click", (e) => {
		
		//მინიმალური შემოწმებით(ოღონდ რაიმე მაინც ეწეროს ეწეროს), ვაგზავნი სერვერზე
		if(domElements.messageText.value.length > 10)
		{
			ws.send(domElements.messageText.value )	
		}
		
	})
	
	connectOrCreatenewRoom.addEventListener("click", (e) => {
		
		if(socketConnected == false) return
		
		var roomId = parseInt(roomIdText.value)
		
		//თუ ოთახის იდენტიფიკატორი არ არის შეყვანილი ან არის არასწორად 
		//მითითებული მაშნ გაიგზავნოს ახალი ოთახის შექმნის მოთხოვნა
		if(roomIdText.value.length < 2 || roomId < MINIMUM_ROOM_ID) {
			
			let message = <Messaging.ServerMessage>{
				
				serverOperation:Messaging.ServerOperations.CREATE_ROOM
								
			}
			
			ws.send( JSON.stringify(message) )
			
		} else {
			
			let message = <Messaging.ServerMessageJoinRoom>{
				
				serverOperation:Messaging.ServerOperations.JOIN_ROOM,
				data:{
					
					roomId:roomId
					
				}
								
			}
			
			ws.send( JSON.stringify(message) )			
			
		}
		
	})	

}

window.onload = (e) => {

	main()
	
}