**WebRTC** sample application with **NodeJS** server side back end, written in **TypeScript**.

Execute:

node server.js

Enter localhost:8080 in browser.